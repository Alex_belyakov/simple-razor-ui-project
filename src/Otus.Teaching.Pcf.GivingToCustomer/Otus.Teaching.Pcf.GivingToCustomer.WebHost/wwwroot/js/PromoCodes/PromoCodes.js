﻿
$(document).ready(function () {
    $('#promocodesTab').addClass('active')
    $('#deleteBtn').click(function (e) {
        e.preventDefault();
        $.ajax({
            url: $('#deleteBtn').data("url"),
            type: "POST",
            data: {
                id: $('#deleteBtn').data("item-id")
            },
            dataType: "html",
            success: function (data, textStatus) {
                $('#itemsContainer').html(data);
                showMessage("Success", "Item deleted");
            },
            error: function (xhr, status, error) {
                window.location.href = "/error";
            }
        });
    });
});

var viewDetails = function (code, serviceInfo, beginDate, endDate, partner, preference) {
    $('#code').text(code);
    $('#serviceInfo').text(serviceInfo);
    $('#beginDate').text(beginDate);
    $('#endDate').text(endDate);
    $('#partner').text(partner);
    $('#preference').text(preference);
    $('#detailsView').modal('show');
}

var hideView = function () {
    $('#detailsView').modal('hide');
}

function showMessage(title, message) {
    $('#messageBox').find('.modal-title').text(title);
    $('#messageBox').find('.modal-body').text(message);
    $('#messageBox').modal('show')
}

var hideMessage = function (title, message) {
    $('#messageBox').modal('hide')
}