﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Otus.Teaching.Pcf.GivingToCustomer.WebHost.Models
{
    public class PromoCodeResponse
    {
        [Required]
        public Guid Id { get; set; }

        [Required]
        public string Code { get; set; }

        [Required]
        public string ServiceInfo { get; set; }

        [Required]
        public string BeginDate { get; set; }

        [Required]
        public string EndDate { get; set; }

        [Required]
        public string Partner { get; set; }
        
        [Required]
        public string Preference { get; set; }
    }
}