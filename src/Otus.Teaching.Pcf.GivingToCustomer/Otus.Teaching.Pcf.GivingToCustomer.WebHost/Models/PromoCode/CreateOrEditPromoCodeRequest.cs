﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Otus.Teaching.Pcf.GivingToCustomer.WebHost.Models
{
    public class CreateOrEditPromoCodeRequest
    {
        [Required]
        public string Code { get; set; }

        [Required]
        public string ServiceInfo { get; set; }

        [Required]
        [DataType(DataType.Date)]
        public string BeginDate { get; set; }

        [Required]
        [DataType(DataType.Date)]
        public string EndDate { get; set; }

        [Required]
        public Guid PartnerId { get; set; }

        [Required]
        public Guid PreferenceId { get; set; }
    }
}