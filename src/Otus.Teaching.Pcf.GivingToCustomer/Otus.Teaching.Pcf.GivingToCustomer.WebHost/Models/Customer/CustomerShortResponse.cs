﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Otus.Teaching.Pcf.GivingToCustomer.WebHost.Models
{
    public class CustomerShortResponse
    {
        [Required]
        public Guid Id { get; set; }
        [Required]
        public string FirstName { get; set; }
        [Required]
        public string LastName { get; set; }
        [Required]
        public string Email { get; set; }
    }
}