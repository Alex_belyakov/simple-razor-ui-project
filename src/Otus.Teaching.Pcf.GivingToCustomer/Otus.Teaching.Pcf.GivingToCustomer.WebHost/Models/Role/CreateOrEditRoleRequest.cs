﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Otus.Teaching.Pcf.GivingToCustomer.WebHost.Models
{
    public class CreateOrEditRoleRequest
    {
        [Required]
        public string Name { get; set; }
    }
}