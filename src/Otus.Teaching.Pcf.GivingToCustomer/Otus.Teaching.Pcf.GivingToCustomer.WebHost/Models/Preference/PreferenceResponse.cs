﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Otus.Teaching.Pcf.GivingToCustomer.WebHost.Models
{
    public class PreferenceResponse
    {
        [Required]
        public Guid Id { get; set; }
        [Required]

        public string Name { get; set; }
    }
}