﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

using Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;
using Otus.Teaching.Pcf.GivingToCustomer.WebHost.Mappers;
using Otus.Teaching.Pcf.GivingToCustomer.WebHost.Models;

namespace Otus.Teaching.Pcf.GivingToCustomer.WebHost.Controllers
{
    public class CustomersController
        : Controller
    {
        private readonly IRepository<Customer> _customerRepository;
        private readonly IRepository<Preference> _preferenceRepository;

        public CustomersController(IRepository<Customer> customerRepository, 
            IRepository<Preference> preferenceRepository)
        {
            _customerRepository = customerRepository;
            _preferenceRepository = preferenceRepository;
        }
        
        public async Task<IActionResult> IndexAsync()
        {
            var customers =  await _customerRepository.GetWithAsync(c => c.Include(c => c.Preferences));
            var list = new List<CustomerResponse>();

            foreach (var customer in customers) 
            {
                var response = new CustomerResponse()
                {
                    Id = customer.Id,
                    Email = customer.Email,
                    FirstName = customer.FirstName,
                    LastName = customer.LastName,
                    Preferences = customer.Preferences
                        .Select(p => new PreferenceResponse { Id = p.PreferenceId, Name = p.Preference.Name })
                        .ToList()
                };
                list.Add(response);
            }

            return View(list);
        }
        
        public async Task<IActionResult> DetailsAsync(Guid id)
        {
            var customer =  await _customerRepository.FirstOrDefaultWithAsync(c => c.Include(ci => ci.Preferences).Where(ci => ci.Id == id));

            var response = new CustomerResponse(customer);

            return View("Details", response);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> CreateAsync(CreateOrEditCustomerRequest request)
        {
            //Получаем предпочтения из бд и сохраняем большой объект
            var preferences = await _preferenceRepository
                .GetRangeByIdsAsync(request.PreferenceIds);

            Customer customer = CustomerMapper.MapFromModel(request, preferences);

            await _customerRepository.AddAsync(customer);

            return await DetailsAsync(customer.Id);
        }

        public async Task<IActionResult> CreateViewAsync()
        {
            var createModel = new CreateOrEditCustomerRequest();
            var preferences = await _preferenceRepository.GetAllAsync();

            ViewBag.Preferences = preferences;

            return View("Create", createModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> EditAsync(Guid id, CreateOrEditCustomerRequest request)
        {
            var customer = await _customerRepository
                .FirstOrDefaultWithAsync(c => c.Include(c => c.Preferences).Where(c => c.Id == id));
            
            if (customer == null)
                throw new ArgumentNullException(nameof(customer));

            var preferences = await _preferenceRepository.GetRangeByIdsAsync(request.PreferenceIds);
            
            CustomerMapper.MapFromModel(request, preferences, customer);

            await _customerRepository.UpdateAsync(customer);

            return await DetailsAsync(customer.Id);
        }

        public async Task<IActionResult> EditViewAsync(Guid id)
        {
            var customer = await _customerRepository.GetByIdAsync(id);
            var requestModel = new CreateOrEditCustomerRequest()
            {
                FirstName = customer.FirstName, 
                LastName = customer.LastName, 
                Email = customer.Email
            };
            var preferences = await _preferenceRepository.GetAllAsync();

            ViewBag.Preferences = preferences;
            ViewBag.Id = customer.Id;

            return View("Edit", requestModel);
        }

        [HttpPost]
        public async Task<IActionResult> DeleteAsync(Guid id)
        {
            var customer = await _customerRepository.GetByIdAsync(id);
            
            if (customer == null)
                throw new ArgumentNullException(nameof(customer));

            await _customerRepository.DeleteAsync(customer);

            return await PartialListAsync();
        }

        public async Task<IActionResult> PartialListAsync()
        {
            var customers = await _customerRepository.GetWithAsync(c => c.Include(c => c.Preferences));
            var list = new List<CustomerResponse>();

            foreach (var customer in customers)
            {
                var response = new CustomerResponse()
                {
                    Id = customer.Id,
                    Email = customer.Email,
                    FirstName = customer.FirstName,
                    LastName = customer.LastName,
                    Preferences = customer.Preferences
                        .Select(p => new PreferenceResponse { Id = p.PreferenceId, Name = p.Preference.Name })
                        .ToList()
                };
                list.Add(response);
            }

            return PartialView("PartialList", list);
        }
    }
}