﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;
using Otus.Teaching.Pcf.GivingToCustomer.WebHost.Models;

namespace Otus.Teaching.Pcf.GivingToCustomer.WebHost.Controllers
{
    public class RolesController
        : Controller
    {
        private readonly IRepository<Role> _rolesRepository;

        public RolesController(IRepository<Role> rolesRepository)
        {
            _rolesRepository = rolesRepository;
        }

        public async Task<IActionResult> IndexAsync()
        {
            var customers = await _rolesRepository.GetAllAsync();

            var response = customers.Select(x => new RoleResponse()
            {
                Id = x.Id,
                Name = x.Name
            });

            return View(response);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> CreateAsync(CreateOrEditRoleRequest request)
        {
            var role = new Role { Name = request.Name };

            await _rolesRepository.AddAsync(role);

            return await DetailsAsync(role.Id);
        }

        public IActionResult CreateView()
        {
            var createModel = new CreateOrEditRoleRequest();

            return View("Create", createModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> EditAsync(Guid id, CreateOrEditRoleRequest request)
        {
            var role = await _rolesRepository
               .GetByIdAsync(id);
            role.Name = request.Name;

            if (role == null)
                throw new ArgumentNullException(nameof(role));

            await _rolesRepository.UpdateAsync(role);

            return await DetailsAsync(role.Id);
        }

        public async Task<IActionResult> EditViewAsync(Guid id)
        {
            var role = await _rolesRepository.GetByIdAsync(id);
            var requestModel = new CreateOrEditRoleRequest()
            {
                Name = role.Name
            };

            ViewBag.Id = role.Id;

            return View("Edit", requestModel);
        }

        [HttpPost]
        public async Task<IActionResult> DeleteAsync(Guid id)
        {
            var role = await _rolesRepository.GetByIdAsync(id);
            await _rolesRepository.DeleteAsync(role);

            return await PartialListAsync();
        }

        public async Task<IActionResult> PartialListAsync()
        {
            var roles = await _rolesRepository.GetAllAsync();

            var response = roles.Select(x => new RoleResponse()
            {
                Id = x.Id,
                Name = x.Name
            }).ToList();

            return PartialView("PartialList", response);
        }

        public async Task<IActionResult> DetailsAsync(Guid id)
        {
            var role = await _rolesRepository.GetByIdAsync(id);

            var response = new RoleResponse { Id = role.Id, Name = role.Name };

            return View("Details", response);
        }
    }
}