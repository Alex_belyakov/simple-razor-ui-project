﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;
using Otus.Teaching.Pcf.GivingToCustomer.WebHost.Models;

namespace Otus.Teaching.Pcf.GivingToCustomer.WebHost.Controllers
{
    public class PreferencesController
        : Controller
    {
        private readonly IRepository<Preference> _preferencesRepository;

        public PreferencesController(IRepository<Preference> preferencesRepository)
        {
            _preferencesRepository = preferencesRepository;
        }

        public async Task<IActionResult> IndexAsync()
        {
            var customers = await _preferencesRepository.GetAllAsync();

            var response = customers.Select(x => new PreferenceResponse()
            {
                Id = x.Id,
                Name = x.Name
            });

            return View(response);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> CreateAsync(CreateOrEditPreferenceRequest request)
        {
            var preference = new Preference { Name = request.Name };

            await _preferencesRepository.AddAsync(preference);

            return await DetailsAsync(preference.Id);
        }

        public IActionResult CreateView()
        {
            var createModel = new CreateOrEditPreferenceRequest();

            return View("Create", createModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> EditAsync(Guid id, CreateOrEditPreferenceRequest request)
        {
            var preference = await _preferencesRepository
               .GetByIdAsync(id);
            preference.Name = request.Name;

            if (preference == null)
                throw new ArgumentNullException(nameof(preference));

            await _preferencesRepository.UpdateAsync(preference);

            return await DetailsAsync(preference.Id);
        }

        public async Task<IActionResult> EditViewAsync(Guid id)
        {
            var preference = await _preferencesRepository.GetByIdAsync(id);
            var requestModel = new CreateOrEditPreferenceRequest()
            {
                Name = preference.Name
            };

            ViewBag.Id = preference.Id;

            return View("Edit", requestModel);
        }

        [HttpPost]
        public async Task<IActionResult> DeleteAsync(Guid id)
        {
            var preference = await _preferencesRepository.GetByIdAsync(id);
            await _preferencesRepository.DeleteAsync(preference);

            return await PartialListAsync();
        }

        public async Task<IActionResult> PartialListAsync()
        {
            var customers = await _preferencesRepository.GetAllAsync();

            var response = customers.Select(x => new PreferenceResponse()
            {
                Id = x.Id,
                Name = x.Name
            }).ToList();

            return PartialView("PartialList", response);
        }

        public async Task<IActionResult> DetailsAsync(Guid id)
        {
            var preference = await _preferencesRepository.GetByIdAsync(id);

            var response = new PreferenceResponse { Id = preference.Id, Name = preference.Name };

            return View("Details", response);
        }
    }
}