﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;
using Otus.Teaching.Pcf.GivingToCustomer.WebHost.Mappers;
using Otus.Teaching.Pcf.GivingToCustomer.WebHost.Models;

namespace Otus.Teaching.Pcf.GivingToCustomer.WebHost.Controllers
{    
    public class PromocodesController
        : Controller
    {
        private readonly IRepository<PromoCode> _promoCodesRepository;
        private readonly IRepository<Preference> _preferencesRepository;
        private readonly IRepository<Customer> _customersRepository;
        private readonly IRepository<Employee> _employeeRepository;

        public PromocodesController(IRepository<PromoCode> promoCodesRepository, IRepository<Preference> preferencesRepository, 
            IRepository<Customer> customersRepository, IRepository<Employee> employeeRepository)
        {
            _promoCodesRepository = promoCodesRepository;
            _preferencesRepository = preferencesRepository;
            _customersRepository = customersRepository;
            _employeeRepository = employeeRepository;
        }

        public async Task<IActionResult> IndexAsync()
        {
            var promoCodes = await _promoCodesRepository.GetAllAsync();
            var codesList = new List<PromoCodeResponse>();

            foreach (var promoCode in promoCodes)
            {
                var preference = await _preferencesRepository.GetByIdAsync(promoCode.PreferenceId);
                var partner = await _employeeRepository.GetByIdAsync(promoCode.PartnerId);
                var response = new PromoCodeResponse()
                {
                    Id = promoCode.Id,
                    BeginDate = promoCode.BeginDate.ToShortDateString(),
                    EndDate = promoCode.EndDate.ToShortDateString(),
                    Code = promoCode.Code,
                    ServiceInfo = promoCode.ServiceInfo,
                    Partner = partner.FullName, 
                    Preference = preference.Name
                };
                codesList.Add(response);
            }

            return View(codesList);
        }

        public async Task<IActionResult> DetailsAsync(Guid id)
        {
            var promoCode = await _promoCodesRepository.GetByIdAsync(id);
            var preference = await _preferencesRepository.GetByIdAsync(promoCode.PreferenceId);
            var partner = await _employeeRepository.GetByIdAsync(promoCode.PartnerId);
            var response = new PromoCodeResponse()
            {
                Id = promoCode.Id,
                BeginDate = promoCode.BeginDate.ToShortDateString(),
                EndDate = promoCode.EndDate.ToShortDateString(),
                Code = promoCode.Code,
                ServiceInfo = promoCode.ServiceInfo,
                Partner = partner.FullName,
                Preference = preference.Name
            };

            return View("Details", response);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> CreateAsync(CreateOrEditPromoCodeRequest request)
        {
            PromoCode promoCode = new PromoCode
            {
                PartnerId = request.PartnerId, 
                ServiceInfo = request.ServiceInfo,
                PreferenceId = request.PreferenceId, 
                BeginDate = DateTime.Parse(request.BeginDate), 
                EndDate = DateTime.Parse(request.EndDate), Code = request.Code, 
                Customers = new List<PromoCodeCustomer>()
            };

            await _promoCodesRepository.AddAsync(promoCode);

            return await DetailsAsync(promoCode.Id);
        }

        public async Task<IActionResult> CreateViewAsync()
        {
            var createModel = new CreateOrEditPromoCodeRequest();
            var preferences = await _preferencesRepository.GetAllAsync();
            var partners = await _employeeRepository.GetAllAsync();

            ViewBag.Preferences = preferences;
            ViewBag.Partners = partners;

            return View("Create", createModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> EditAsync(Guid id, CreateOrEditPromoCodeRequest request)
        {
            var promoCode = await _promoCodesRepository.GetByIdAsync(id);

            promoCode.PartnerId = request.PartnerId;
            promoCode.PreferenceId = request.PreferenceId;
            promoCode.ServiceInfo = request.ServiceInfo;
            promoCode.Code = request.Code;
            promoCode.BeginDate = DateTime.Parse(request.BeginDate);
            promoCode.EndDate = DateTime.Parse(request.EndDate);

            await _promoCodesRepository.UpdateAsync(promoCode);

            return RedirectToAction("Details", new { id = promoCode.Id });
        }

        public async Task<IActionResult> EditViewAsync(Guid id)
        {
            var promoCode = await _promoCodesRepository.GetByIdAsync(id);
            var preference = await _preferencesRepository.GetByIdAsync(promoCode.PreferenceId);
            var partner = await _employeeRepository.GetByIdAsync(promoCode.PartnerId);
            var requestModel = new CreateOrEditPromoCodeRequest()
            {
                BeginDate = promoCode.BeginDate.ToShortDateString(),
                EndDate = promoCode.EndDate.ToShortDateString(),
                Code = promoCode.Code,
                ServiceInfo = promoCode.ServiceInfo,
                PartnerId = partner.Id,
                PreferenceId = preference.Id
            };
            var preferences = await _preferencesRepository.GetAllAsync();
            var partners = await _employeeRepository.GetAllAsync();

            ViewBag.Preferences = preferences;
            ViewBag.Partners = partners;
            ViewBag.Id = id;

            return View("Edit", requestModel);
        }

        [HttpPost]
        public async Task<IActionResult> DeleteAsync(Guid id)
        {
            var promoCode = await _promoCodesRepository.GetByIdAsync(id);

            if (promoCode == null)
                throw new ArgumentNullException(nameof(promoCode));

            await _promoCodesRepository.DeleteAsync(promoCode);

            return await PartialListAsync();
        }

        public async Task<IActionResult> PartialListAsync()
        {
            var promoCodes = await _promoCodesRepository.GetAllAsync();
            var codesList = new List<PromoCodeResponse>();

            foreach (var promoCode in promoCodes)
            {
                var preference = await _preferencesRepository.GetByIdAsync(promoCode.PreferenceId);
                var partner = await _employeeRepository.GetByIdAsync(promoCode.Id);
                var response = new PromoCodeResponse()
                {
                    Id = promoCode.Id,
                    BeginDate = promoCode.BeginDate.ToShortDateString(),
                    EndDate = promoCode.EndDate.ToShortDateString(),
                    Code = promoCode.Code,
                    ServiceInfo = promoCode.ServiceInfo,
                    Partner = partner.FullName,
                    Preference = preference.Name
                };
                codesList.Add(response);
            }

            return View("PartialList", codesList);
        }
    }
}