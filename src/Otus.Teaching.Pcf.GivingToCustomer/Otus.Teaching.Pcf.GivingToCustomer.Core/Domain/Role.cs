﻿using System;

namespace Otus.Teaching.Pcf.GivingToCustomer.Core.Domain
{
    public class Role
        : BaseEntity
    {
        public string Name { get; set; }

        public string Description { get; set; }
    }
}